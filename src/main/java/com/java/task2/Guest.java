package com.java.task2;

/**
 * Is is a Guest model.
 * Model can tell and hear a rumor.
 * @author Ruslan Tuz
 */
public class Guest {
    /**
     * This class describes if guest is already heard rumor.
     * If guest is already heard rumor the game will end. @see RummorGame class
     */
    private boolean alreadyHeard;

    /**
     * @return is guest already heard the rumor
     */
    public boolean isAlreadyHeard() {
        return alreadyHeard;
    }

    /**
     * Tell rumor to another guest
     * @param guestTellTo guest to tell the rumor
     */
    public void tellRumor(Guest guestTellTo) {
        guestTellTo.hearRumor();
    }

    /**
     * Guest hear the rumor, and flag alreadyHeard is changing to true
     */
    public void hearRumor() {
        alreadyHeard = true;
    }
}
