package com.java.task2;

import java.util.List;
import java.util.Random;

/**
 * This class describes the game model.
 * Class accepts list of guests and can start a game.
 * @author Ruslan Tuz
 */
public class RumorGame {
    /**
     * Collection of Guests
     */
    private List<Guest> guests;

    /**
     * Constructor of class
     * @param guests Guest collection
     */
    public RumorGame(List<Guest> guests) {
        this.guests = guests;
    }

    /**
     * Main method that start a game.
     * Every gust will tel a rumor to another until someone hear rumor again.
     * @param firstTeller Guest first to tell rumor.
     * @return count of guest who hear rumor once
     */
    public int playGame(Guest firstTeller) {
        Guest currentTeller = firstTeller;
        firstTeller.hearRumor();
        int heardCount = 1;
        boolean gameInProcess = true;

        while (gameInProcess) {
            Guest guestToTellRumor = getRandomNotCurrentGuest(currentTeller);

            if (guestToTellRumor.isAlreadyHeard()) {
                gameInProcess = false;
            } else {
                currentTeller.tellRumor(guestToTellRumor);
                currentTeller = guestToTellRumor;
            }

            heardCount++;
        }

        return heardCount;
    }

    /**
     * Method is for choosing a random guest from game to tell rumor but not the teller.
     * @param currentTeller Guest not to choose to
     * @return random guest
     */
    private Guest getRandomNotCurrentGuest(Guest currentTeller) {
        Guest guestToTellRumor;
        do {
            guestToTellRumor = guests.get(new Random().nextInt(guests.size()));
        } while (guestToTellRumor.equals(currentTeller));

        return guestToTellRumor;
    }
}
