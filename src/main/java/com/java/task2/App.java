package com.java.task2;

/**
 * Main class, where app starts
 * @author Ruslan Tuz
 */
public class App {

    /**
     * Main function where program starts
     * @param args arguments
     */
    public static void main(String[] args) {
        Alice alice = new Alice();
        alice.startGame();
    }
}
