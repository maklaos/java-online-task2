package com.java.task2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Class describes that ALice trow patry.
 * This class is calculate the probability that everyone in the party will hear the rumor about Alice;
 * @author Ruslan Tuz
 */
public class Alice {
    /**
     * Method to generate guest for a rumor game
     * @param guestsCount number of guests of generate
     * @return collection of guests to join the game
     */
    private List<Guest> generateGuests(int guestsCount) {
        List<Guest> guests = new ArrayList<>();
        for (int i = 1; i < guestsCount; i++) {
            guests.add(new Guest());
        }
        return guests;
    }

    /**
     * This class is used to start a game.
     * This will print results after game.
     */
    public void startGame() {
        int guestsCount = getGuestNumber();
        int rumorHeardCount = 0;
        double experimentsCount = 200;
        for (int i = 0; i < experimentsCount; i++) {
            List<Guest> guests = generateGuests(guestsCount);
            Guest bob = new Guest();
            guests.add(bob);

            RumorGame rumorGame = new RumorGame(guests);
            rumorHeardCount += rumorGame.playGame(bob);
        }
        double averageHear = rumorHeardCount / experimentsCount;
        System.out.println("Probability that everyone at the party (except Alice) will hear the rumor before: " + (averageHear / guestsCount) * 100 + "%");
        System.out.println("Expected number of people to hear the rumor: " + averageHear);
    }

    /**
     * Ask user to enter a nu,ber of guests.
     * @return number of guests user entered
     */
    protected int getGuestNumber() {
        System.out.print("Enter number of guest:");
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }
}
